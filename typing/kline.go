package typing

import (
	"time"
)

//go:generate mockgen -destination=./mocks/kline.go -package=mocks -source=kline.go Kline
type Kline interface {
	Timestamp() time.Time
	BarTime() NumVariable[int64]
	Close() NumVariable[float64]
	High() NumVariable[float64]
	Low() NumVariable[float64]
	Open() NumVariable[float64]
	Volume() NumVariable[float64]
}
