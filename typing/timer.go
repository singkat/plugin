package typing

import "time"

//go:generate mockgen -destination=./mocks/timer.go -package=mocks -source=timer.go Timer

type Timer interface {
	// GetClosesNextTime, it uses the reference time to get future time after some preferred duration.
	// for example:
	//		1. time.Now(): 10:10:10.200, future time for the next 1 second is 10:10:11.000 = ~800ms delta
	//		2. time.Now(): 10:10:10.200, future time for the next 1 Minute is 10:11:00.000 = ~49800ms delta
	GetClosestNextTime(referenceTime time.Time, targetDurationInFuture time.Duration) time.Time
	// After, send an elapsed time to the channel after some preferred duration. Client can then receive it.
	// It is different than the built-in "time.After" which will send elapsed time to the channel with
	// consistent delta interval duration. The behaviour of this "After" is first try to get the closes future time
	// after some preferred duration then utilize it for the next reference,
	// hence delta interval duration for the 1st interval is not the same with the rest of
	// subsequent call to this After method.
	// For example, using built-in "time.After" for 1 sec duration:
	// 		1. reference: 10:10:10.200 => future ~10:10:11.200 = ~1000ms
	// 		2. reference: 10:10:11.200 => future ~10:10:12.200 = ~1000ms
	// while using this After method for the same duration provides:
	// 		1. reference: 10:10:10.200 => future ~10:10:11.000 = ~800ms
	// 		2. reference: ~10:10:11.000 => future ~10:10:12.000 = ~1000ms
	// Another note is that Unix can easily use micro (time.Microsecond) or nanosecond (time.Nanosecond)
	// tick reference while on Windows it is better to set millisecond (time.Millisecond) as tick reference.
	// Behind the scene it is simply utilize built in timer as the vehicle to arrive to destination time
	// and by using tick reference, the precision can be controlled with some tolerance to detect elapsed time.
	// For system critical functionality which require very precise time comparisson
	// it is always better to utilize Golang built in time package as the wall clock
	// and monotonic clock are being taken care more precisely by that package.
	After(intervalDuration time.Duration, tickReference time.Duration, tolerance int64) <-chan time.Time
}
