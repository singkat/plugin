package typing

//go:generate mockgen -destination=./mocks/simple.go -package=mocks -source=simple.go Simple

type Simple interface {
	NewSimpleFloat(name string, value float64) SimpleVariable[float64]
	GetSimpleFloat(name string) SimpleVariable[float64]
	NewSimpleInt(name string, value int64) SimpleVariable[int64]
	GetSimpleInt64(name string) SimpleVariable[int64]
	NewSimpleString(name string, value string) SimpleVariable[string]
	GetSimpleString(name string) SimpleVariable[string]
	NewSimpleBool(name string, value bool) SimpleVariable[bool]
	GetSimpleBool(name string) SimpleVariable[bool]
}
