package typing

import "context"

//go:generate mockgen -destination=./mocks/runner.go -package=mocks -source=runner.go Runner

type InitOption struct {
	Strategy Strategy
	TA       TA
	Input    Simple
	Data     Series
	Window   Window
	Timer    Timer
}

type Runner interface {
	Init(ctx context.Context, opt *InitOption) error
	GetName() string
	GetDefaultInput() map[string]any
	Run(ctx context.Context, kline Kline) error
}
