package typing

//go:generate mockgen -destination=./mocks/reporter.go -package=mocks -source=reporter.go AlertSender DoneReporter

type AlertSender interface {
	Send(topic string, msg any) error
}

type ReportKind string

const (
	ReportKindLog   = "log"
	ReportKindChart = "chart"
)

type DoneReporter interface {
	Done(kind ReportKind, data any) error
}
