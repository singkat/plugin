package typing

//go:generate mockgen -destination=./mocks/strategy.go -package=mocks -source=strategy.go Strategy

type Position int

const (
	LONG_POSITION  Position = 1
	SHORT_POSITION Position = -1
)

type Outcome int

const (
	PROFIT Outcome = 1
	LOSS   Outcome = -1
)

type Action int

const (
	ENTRY Action = 1
	EXIT  Action = -1
)

type Side int

const (
	BUY  Side = 1
	SELL Side = -1
)

type Result struct {
	Outcome Outcome
	Entry   Order
	Exit    Order
	PNL     float64
}

type Order struct {
	ID        string
	PairID    string
	Position  Position
	Action    Action
	Side      Side
	Price     float64
	Qty       float64
	Amount    float64
	Timestamp int64
}

type StrategyPreference struct {
	Quantity float64
	Symbol   string
	Topic    string
}

type Strategy interface {
	SetCurrentClose(close float64)
	Enter(id string, position Position, alertMsg any) string
	Close(id string, alertMsg any) string
	Alert(msg any) error
	Plot(data any) error
	Done(data ...any) error
	CurrentTradeResult() map[string]Result
	GetEntry(id string) *Order
	GetPreference() StrategyPreference
}
