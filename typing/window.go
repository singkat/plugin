package typing

import "time"

//go:generate mockgen -destination=./mocks/window.go -package=mocks -source=window.go Window

// Window provides multi time-window reference
type Window interface {
	OHLCV(timeframe time.Duration, until time.Time) Kline
}
