package typing

import "golang.org/x/exp/constraints"

// the purpose of all below interfaces is to encapsulate data series index operation,
// series manipulation, to avoid unnecessary additional iteration for sorting and moving window operation
// as part of time critical optimization of statistic calculation (significantly for large series).
// The drawback is that it uses more allocations for the execution

type VariableIndexSynchronizer interface {
	SetCurrentIndex(i int)
}

type SimpleVariable[T any] interface {
	Val() T
	Key() string
}

type NonSerialVariable[T any] interface {
	Set(val T)
	Val() T
}

type GenericVariable[T any] interface {
	VariableIndexSynchronizer

	Set(val T)
	Val(index int) T
	Series() []T
}

type NumVariable[T constraints.Float | constraints.Integer] interface {
	GenericVariable[T]

	Add(right NumVariable[T]) NumVariable[T]
	Sub(right NumVariable[T]) NumVariable[T]
	Div(right NumVariable[T]) NumVariable[T]
	Mult(right NumVariable[T]) NumVariable[T]
	MultBy(multiplier T) NumVariable[T]
}

type FloatVariable[T float64] interface {
	NumVariable[T]

	SetPrecision(prec int)
}

type StrVariable[T string] interface {
	GenericVariable[T]

	Concat(right StrVariable[T]) StrVariable[T]
	Substring(search, replace string) StrVariable[T]
}

type ArrayVariable[T any] interface {
	VariableIndexSynchronizer

	Push(GenericVariable[T])
	Pop() GenericVariable[T]
	Get(index int) GenericVariable[T]
	List() []GenericVariable[T]
}

type ArrayUtil[T any] interface {
	Join(vars []GenericVariable[T], delimiter string) string
	Concat(strVars []GenericVariable[string]) string
	Split(strVars []GenericVariable[string]) []string
}
