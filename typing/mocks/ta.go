// Code generated by MockGen. DO NOT EDIT.
// Source: ta.go

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockTA is a mock of TA interface.
type MockTA struct {
	ctrl     *gomock.Controller
	recorder *MockTAMockRecorder
}

// MockTAMockRecorder is the mock recorder for MockTA.
type MockTAMockRecorder struct {
	mock *MockTA
}

// NewMockTA creates a new mock instance.
func NewMockTA(ctrl *gomock.Controller) *MockTA {
	mock := &MockTA{ctrl: ctrl}
	mock.recorder = &MockTAMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockTA) EXPECT() *MockTAMockRecorder {
	return m.recorder
}

// ADX mocks base method.
func (m *MockTA) ADX(closes, highs, lows []float64, adxLen, diLen int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ADX", closes, highs, lows, adxLen, diLen)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// ADX indicates an expected call of ADX.
func (mr *MockTAMockRecorder) ADX(closes, highs, lows, adxLen, diLen interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ADX", reflect.TypeOf((*MockTA)(nil).ADX), closes, highs, lows, adxLen, diLen)
}

// ATR mocks base method.
func (m *MockTA) ATR(closes, highs, lows []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ATR", closes, highs, lows, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// ATR indicates an expected call of ATR.
func (mr *MockTAMockRecorder) ATR(closes, highs, lows, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ATR", reflect.TypeOf((*MockTA)(nil).ATR), closes, highs, lows, length)
}

// ATRStopLossFinder mocks base method.
func (m *MockTA) ATRStopLossFinder(closes, highs, lows []float64, length int, multiplier float64) ([]float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ATRStopLossFinder", closes, highs, lows, length, multiplier)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	return ret0, ret1
}

// ATRStopLossFinder indicates an expected call of ATRStopLossFinder.
func (mr *MockTAMockRecorder) ATRStopLossFinder(closes, highs, lows, length, multiplier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ATRStopLossFinder", reflect.TypeOf((*MockTA)(nil).ATRStopLossFinder), closes, highs, lows, length, multiplier)
}

// AccDist mocks base method.
func (m *MockTA) AccDist(closes, highs, lows, volumes []float64) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "AccDist", closes, highs, lows, volumes)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// AccDist indicates an expected call of AccDist.
func (mr *MockTAMockRecorder) AccDist(closes, highs, lows, volumes interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "AccDist", reflect.TypeOf((*MockTA)(nil).AccDist), closes, highs, lows, volumes)
}

// BB mocks base method.
func (m *MockTA) BB(series []float64, length int, mult float64) ([]float64, []float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "BB", series, length, mult)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	ret2, _ := ret[2].([]float64)
	return ret0, ret1, ret2
}

// BB indicates an expected call of BB.
func (mr *MockTAMockRecorder) BB(series, length, mult interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "BB", reflect.TypeOf((*MockTA)(nil).BB), series, length, mult)
}

// CE mocks base method.
func (m *MockTA) CE(closes, highs, lows []float64, length, mult int) ([]float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CE", closes, highs, lows, length, mult)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	return ret0, ret1
}

// CE indicates an expected call of CE.
func (mr *MockTAMockRecorder) CE(closes, highs, lows, length, mult interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CE", reflect.TypeOf((*MockTA)(nil).CE), closes, highs, lows, length, mult)
}

// CO mocks base method.
func (m *MockTA) CO(closes, highs, lows, volumes []float64, fast, slow int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CO", closes, highs, lows, volumes, fast, slow)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// CO indicates an expected call of CO.
func (mr *MockTAMockRecorder) CO(closes, highs, lows, volumes, fast, slow interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CO", reflect.TypeOf((*MockTA)(nil).CO), closes, highs, lows, volumes, fast, slow)
}

// DC mocks base method.
func (m *MockTA) DC(highs, lows []float64, length int) ([]float64, []float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "DC", highs, lows, length)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	ret2, _ := ret[2].([]float64)
	return ret0, ret1, ret2
}

// DC indicates an expected call of DC.
func (mr *MockTAMockRecorder) DC(highs, lows, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "DC", reflect.TypeOf((*MockTA)(nil).DC), highs, lows, length)
}

// EMA mocks base method.
func (m *MockTA) EMA(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "EMA", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// EMA indicates an expected call of EMA.
func (mr *MockTAMockRecorder) EMA(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "EMA", reflect.TypeOf((*MockTA)(nil).EMA), series, length)
}

// Highest mocks base method.
func (m *MockTA) Highest(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Highest", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// Highest indicates an expected call of Highest.
func (mr *MockTAMockRecorder) Highest(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Highest", reflect.TypeOf((*MockTA)(nil).Highest), series, length)
}

// Lowest mocks base method.
func (m *MockTA) Lowest(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Lowest", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// Lowest indicates an expected call of Lowest.
func (mr *MockTAMockRecorder) Lowest(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Lowest", reflect.TypeOf((*MockTA)(nil).Lowest), series, length)
}

// MACD mocks base method.
func (m *MockTA) MACD(series []float64, fastLen, slowLen, sigLen int) ([]float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "MACD", series, fastLen, slowLen, sigLen)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	return ret0, ret1
}

// MACD indicates an expected call of MACD.
func (mr *MockTAMockRecorder) MACD(series, fastLen, slowLen, sigLen interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "MACD", reflect.TypeOf((*MockTA)(nil).MACD), series, fastLen, slowLen, sigLen)
}

// NWREnvelope mocks base method.
func (m *MockTA) NWREnvelope(dataSeries []float64, windowSize int, kernelBandwidth, multiplier float64) ([]float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "NWREnvelope", dataSeries, windowSize, kernelBandwidth, multiplier)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	return ret0, ret1
}

// NWREnvelope indicates an expected call of NWREnvelope.
func (mr *MockTAMockRecorder) NWREnvelope(dataSeries, windowSize, kernelBandwidth, multiplier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "NWREnvelope", reflect.TypeOf((*MockTA)(nil).NWREnvelope), dataSeries, windowSize, kernelBandwidth, multiplier)
}

// NWREstimates mocks base method.
func (m *MockTA) NWREstimates(dataSeries []float64, windowSize int, kernelBandwidth, multiplier float64) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "NWREstimates", dataSeries, windowSize, kernelBandwidth, multiplier)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// NWREstimates indicates an expected call of NWREstimates.
func (mr *MockTAMockRecorder) NWREstimates(dataSeries, windowSize, kernelBandwidth, multiplier interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "NWREstimates", reflect.TypeOf((*MockTA)(nil).NWREstimates), dataSeries, windowSize, kernelBandwidth, multiplier)
}

// OBV mocks base method.
func (m *MockTA) OBV(volumes []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "OBV", volumes, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// OBV indicates an expected call of OBV.
func (mr *MockTAMockRecorder) OBV(volumes, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "OBV", reflect.TypeOf((*MockTA)(nil).OBV), volumes, length)
}

// PSAR mocks base method.
func (m *MockTA) PSAR(closes, highs, lows []float64, length int, start, incr, max float64) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "PSAR", closes, highs, lows, length, start, incr, max)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// PSAR indicates an expected call of PSAR.
func (mr *MockTAMockRecorder) PSAR(closes, highs, lows, length, start, incr, max interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "PSAR", reflect.TypeOf((*MockTA)(nil).PSAR), closes, highs, lows, length, start, incr, max)
}

// RMA mocks base method.
func (m *MockTA) RMA(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RMA", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// RMA indicates an expected call of RMA.
func (mr *MockTAMockRecorder) RMA(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RMA", reflect.TypeOf((*MockTA)(nil).RMA), series, length)
}

// RSI mocks base method.
func (m *MockTA) RSI(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "RSI", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// RSI indicates an expected call of RSI.
func (mr *MockTAMockRecorder) RSI(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "RSI", reflect.TypeOf((*MockTA)(nil).RSI), series, length)
}

// SMA mocks base method.
func (m *MockTA) SMA(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SMA", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// SMA indicates an expected call of SMA.
func (mr *MockTAMockRecorder) SMA(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SMA", reflect.TypeOf((*MockTA)(nil).SMA), series, length)
}

// SMMA mocks base method.
func (m *MockTA) SMMA(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SMMA", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// SMMA indicates an expected call of SMMA.
func (mr *MockTAMockRecorder) SMMA(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SMMA", reflect.TypeOf((*MockTA)(nil).SMMA), series, length)
}

// SMMA3 mocks base method.
func (m *MockTA) SMMA3(series []float64, length1, length2, length3 int) ([]float64, []float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "SMMA3", series, length1, length2, length3)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	ret2, _ := ret[2].([]float64)
	return ret0, ret1, ret2
}

// SMMA3 indicates an expected call of SMMA3.
func (mr *MockTAMockRecorder) SMMA3(series, length1, length2, length3 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SMMA3", reflect.TypeOf((*MockTA)(nil).SMMA3), series, length1, length2, length3)
}

// Stdev mocks base method.
func (m *MockTA) Stdev(series []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Stdev", series, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// Stdev indicates an expected call of Stdev.
func (mr *MockTAMockRecorder) Stdev(series, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Stdev", reflect.TypeOf((*MockTA)(nil).Stdev), series, length)
}

// Stoch mocks base method.
func (m *MockTA) Stoch(closes, highs, lows []float64, length int) []float64 {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Stoch", closes, highs, lows, length)
	ret0, _ := ret[0].([]float64)
	return ret0
}

// Stoch indicates an expected call of Stoch.
func (mr *MockTAMockRecorder) Stoch(closes, highs, lows, length interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Stoch", reflect.TypeOf((*MockTA)(nil).Stoch), closes, highs, lows, length)
}

// StochRSI mocks base method.
func (m *MockTA) StochRSI(closes, highs, lows []float64, kLen, dLen, stochLen, rsiLen int) ([]float64, []float64) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "StochRSI", closes, highs, lows, kLen, dLen, stochLen, rsiLen)
	ret0, _ := ret[0].([]float64)
	ret1, _ := ret[1].([]float64)
	return ret0, ret1
}

// StochRSI indicates an expected call of StochRSI.
func (mr *MockTAMockRecorder) StochRSI(closes, highs, lows, kLen, dLen, stochLen, rsiLen interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "StochRSI", reflect.TypeOf((*MockTA)(nil).StochRSI), closes, highs, lows, kLen, dLen, stochLen, rsiLen)
}
