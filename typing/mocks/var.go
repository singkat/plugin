// Code generated by MockGen. DO NOT EDIT.
// Source: var.go

// Package mocks is a generated GoMock package.
package mocks

import (
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockVariableIndexSynchronizer is a mock of VariableIndexSynchronizer interface.
type MockVariableIndexSynchronizer struct {
	ctrl     *gomock.Controller
	recorder *MockVariableIndexSynchronizerMockRecorder
}

// MockVariableIndexSynchronizerMockRecorder is the mock recorder for MockVariableIndexSynchronizer.
type MockVariableIndexSynchronizerMockRecorder struct {
	mock *MockVariableIndexSynchronizer
}

// NewMockVariableIndexSynchronizer creates a new mock instance.
func NewMockVariableIndexSynchronizer(ctrl *gomock.Controller) *MockVariableIndexSynchronizer {
	mock := &MockVariableIndexSynchronizer{ctrl: ctrl}
	mock.recorder = &MockVariableIndexSynchronizerMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockVariableIndexSynchronizer) EXPECT() *MockVariableIndexSynchronizerMockRecorder {
	return m.recorder
}

// SetCurrentIndex mocks base method.
func (m *MockVariableIndexSynchronizer) SetCurrentIndex(i int) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "SetCurrentIndex", i)
}

// SetCurrentIndex indicates an expected call of SetCurrentIndex.
func (mr *MockVariableIndexSynchronizerMockRecorder) SetCurrentIndex(i interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "SetCurrentIndex", reflect.TypeOf((*MockVariableIndexSynchronizer)(nil).SetCurrentIndex), i)
}
