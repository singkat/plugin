package typing

//go:generate mockgen -destination=./mocks/ta.go -package=mocks -source=ta.go TA

type TA interface {
	AccDist(closes, highs, lows, volumes []float64) []float64
	ADX(closes, highs, lows []float64, adxLen, diLen int) []float64
	ATR(closes, highs, lows []float64, length int) []float64
	BB(series []float64, length int, mult float64) ([]float64, []float64, []float64)
	CE(closes, highs, lows []float64, length, mult int) ([]float64, []float64)
	CO(closes, highs, lows, volumes []float64, fast, slow int) []float64
	DC(highs, lows []float64, length int) ([]float64, []float64, []float64)
	EMA(series []float64, length int) []float64
	Highest(series []float64, length int) []float64
	Lowest(series []float64, length int) []float64
	MACD(series []float64, fastLen, slowLen, sigLen int) ([]float64, []float64)
	OBV(volumes []float64, length int) []float64
	PSAR(closes []float64, highs []float64, lows []float64, length int, start, incr, max float64) []float64
	RMA(series []float64, length int) []float64
	RSI(series []float64, length int) []float64
	SMA(series []float64, length int) []float64
	SMMA(series []float64, length int) []float64
	SMMA3(series []float64, length1, length2, length3 int) ([]float64, []float64, []float64)
	Stdev(series []float64, length int) []float64
	StochRSI(closes, highs, lows []float64, kLen, dLen, stochLen, rsiLen int) ([]float64, []float64)
	Stoch(closes, highs, lows []float64, length int) []float64
	NWREnvelope(dataSeries []float64, windowSize int, kernelBandwidth, multiplier float64) ([]float64, []float64)
	NWREstimates(dataSeries []float64, windowSize int, kernelBandwidth, multiplier float64) []float64
	ATRStopLossFinder(closes, highs, lows []float64, length int, multiplier float64) ([]float64, []float64)
}
