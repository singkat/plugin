package typing

//go:generate mockgen -destination=./mocks/data.go -package=mocks -source=data.go Series

type Series interface {
	Next()
	NewFloat(name string, initialValue []float64) NumVariable[float64]
	NewInt(name string, initialValue []int64) NumVariable[int64]
	NewMemoList(name string) NonSerialVariable[any]
	WrapFloat(values ...[]float64) (NumVariable[float64], NumVariable[float64], NumVariable[float64])
	WrapInt(values ...[]int64) (NumVariable[int64], NumVariable[int64], NumVariable[int64])
	Dataseries(v NumVariable[float64], t NumVariable[int64]) []map[string]any
}
