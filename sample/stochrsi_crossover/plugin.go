package sample

import (
	"context"

	t "gitlab.com/singkat/plugin/typing"
)

type StochRSICrossover struct{}

func (rc *StochRSICrossover) GetName() string {
	return "unique_stochrsi_crossover_name"
}

func (rc *StochRSICrossover) Run(ctx context.Context, kline t.Kline, ta t.TA, strategy t.Strategy, input t.Simple, data t.Series, w t.Window) error {
	kLen := int(input.GetSimpleInt64("kLen").Val())
	dLen := int(input.GetSimpleInt64("dLen").Val())
	stochLen := int(input.GetSimpleInt64("stochLen").Val())
	rsiLen := int(input.GetSimpleInt64("rsiLen").Val())

	k, d, _ := data.WrapFloat(ta.StochRSI(
		kline.Close().Series(),
		kline.High().Series(),
		kline.Low().Series(),
		kLen,
		dLen,
		stochLen,
		rsiLen,
	))

	dir := data.NewInt("dir", []int64{})
	if k.Val(1) < d.Val(1) && k.Val(0) > d.Val(0) {
		dir.Set(1)
	}

	if k.Val(1) > d.Val(1) && k.Val(0) < d.Val(0) {
		dir.Set(-1)
	}

	changeDir := data.NewInt("changeDir", []int64{})
	if dir.Val(1) != dir.Val(0) {
		changeDir.Set(1)
	}

	if changeDir.Val(0) == 1 {
		if dir.Val(0) == 1 {
			strategy.Alert("Long")
		}

		if dir.Val(0) == -1 {
			strategy.Alert("Short")
		}
	}

	return nil
}
