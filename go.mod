module gitlab.com/singkat/plugin

go 1.19

require (
	github.com/golang/mock v1.6.0
	golang.org/x/exp v0.0.0-20230626212559-97b1e661b5df
)
